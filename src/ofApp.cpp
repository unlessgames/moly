#include "ofApp.h"

enum State{
  GAME,
  UNLOCK_LERP,
  ENDING,
  END_LERP
};

bool SHOW_FPS = false;
bool SHOW_STATS = false;



ofImage img;
int screenshots = 0;

int width, height;

ofEasyCam cam;

int progress = 0;


bool noinput = true, 
     leftmousedown = false,
     middlemousedown = false,
     leftmouseup = false,
     rightmousedown = false,
     rightmouseup = false;

ofVec3f mouse;
float camsize = 1;

Cross cross = Cross();
Cross target = Cross();
float velocity = 0;
std::vector<Rect> rects;
std::vector<ofVec3f> dots;

ofTrueTypeFont font;

int flash_time = 0;
float flash_duration = 30.0f;
int frame = 0;

int filled = -1;

bool flashed = false;

float color_time = 0.0f;

float w2;
float h2;

PointCloud point_cloud;

State state = GAME;
LerpClock cam_clock = LerpClock(0.2f);

LerpClock hint_clock = LerpClock(0.34f);

LerpClock end_clock = LerpClock(0.08f);

float end_radius = 0.0f;



float idle_time = 0;
float idle_limit = 60.f;


float end_angle = 0.0f;

FpsCounter fps = FpsCounter(8);

Stats stats;

void reset_idle(){
  idle_time = 0.0f;
}

float rx(){
	return ofRandom(-w2, w2);
}

float ry(){
	return ofRandom(-h2, h2);
}

float ran(){
	return ofRandom(0.0f, 1.0f);
}

std::vector<Dragged> dragged_rects;

void reset_camera() {
  cam.setPosition(0, 0, -10);
  cam.setScale(camsize,camsize,1);
  cam.setOrientation(ofVec3f(0, 0, 0));
}

void lerp_unlock(float t){
  float mult = 1.0f;
  float tt = ofClamp(t * 2.0f, 0.0f, 1.0f);
  float x = ofLerp(target.x, 0.0f, tt);
  float y = ofLerp(target.y, 0.0f, tt);
  target.setPos(x, y);
  target.setSize(ofLerp(9.0f, 0.0f, tt));
    // target.setRot(ofLerp(target.r, 0.0f, tt));
  end_radius = ofLerp(0.0f, ofGetHeight() * 0.42f, t);
}


std::vector<Rect> gen_rects(){
	float duci = 180.0f + 50.0f * ran();
	float apro = 100.0f + 30.0f * ran();

	return {
		Rect(
		  rx(), ry(),
	    apro, apro + ran() * 20.0f, 
	    ofRandom(-20.0f, 20.f),
	    DOTTED
		),
		Rect(
		  rx(), ry(),
	    duci, duci + 50 * ran(), 
	    45.0f + ofRandom(-20.0f, 20.f),
	    DOTTED
		),
		Rect(
		  rx(), ry(),
	    60 + ran() * 50, 270 + ran() * 50, 
	    ofRandom(-20.0f, 20.f),
	    STRIPES
		),
		Rect(
		  rx(), ry(),
	    60 + ran() * 50, 270 + ran() * 50, 
	    90.f + ofRandom(-20.0f, 20.f),
	    STRIPES
		)
	};
}

void set_state(State s, bool clock = false){
  state = s;
  if(clock)
    cam_clock.start();
  else
    cam_clock.time = 0.0f;
}


void reset_level(){
  flashed = false;
	velocity = 2.0f;
	target.setPos(rx() * 0.7f, ry() * 0.7f);
	target.setRot(ran() * 360.0f);
  target.setSize(9.0f);
  // init_dots(DOTS);

  point_cloud.set_size(DOTS, ofGetWidth(), ofGetHeight());  
    
  point_cloud.randomize();
	rects = gen_rects();
  filled = NONE;
  end_radius = 0.0f;
  cam_clock.time = 0.0f;
  set_state(GAME);
  color_time = 0.0f;

  stats = Stats();
}

void get_size(){
  width = ofGetScreenWidth();
  height = ofGetScreenHeight();
  w2 = width / 2;
  h2 = height / 2;
}


void ofApp::setup(){
  cam.removeAllInteractions();
  cam.enableOrtho();
  cam.setVFlip(true);

  ofDisableSmoothing();
  ofDisableAntiAliasing();

  ofHideCursor();

  get_size();
  
  point_cloud.init(DOTS, ofGetWidth(), ofGetHeight());

  reset_level();

  reset_camera();
  font.loadFont("terminus.ttf", 13);

}



void ofApp::update(){
  float dt = ofGetLastFrameTime();
  idle_time += dt;
  if (idle_time > idle_limit) {
    reset_idle();
    reset_level();
  }


	mouse = ofVec3f(ofGetMouseX(), ofGetMouseY(), 0);
	mouse = cam.screenToWorld(mouse);

  if(state == ENDING){
    end_clock.update(dt);
    if(end_clock.state == EOC)
      set_state(END_LERP, true);
  }
} 
void draw_frame(int x = 0, int y = 0, bool show_title = SHOW_TITLE){
  ofFill();
  ofSetColor(160);
  ofSetPolyMode(OF_POLY_WINDING_ODD);
  ofBeginShape();
  float w = ofGetWidth();
  float h = ofGetHeight() * 0.5f;
  float s = 30.0f;
  ofVertex(x,y);
  ofVertex(x + w,y);
  ofVertex(x + w,y + h*2);
  ofVertex(x, y + h*2);
  ofVertex(x,y);
  ofVertex(x + s, y + s);
  ofVertex(x + s, y + h*2 - s);
  ofVertex(x + w - s, y + h*2 - s);
  ofVertex(x + w - s, y + s);
  ofVertex(x + s, y + s);
  ofEndShape(true);

  if(show_title){
    ofSetColor(0);
    ofDrawBitmapString("moly=akos_plesnivy+moholy-nagy_laszlo.linocut_I", 
      x + floor(w * 0.5f - 210.0f), 
      y + floor(h * 2 - 10)
    );

  }
}

void draw_arc(float x, float y, float r, float rot = 0.1f, float a = 3.141592f, float color = 200.0f, float res = 64.0f){
  float step = a/res;
  ofSetColor(color);
  ofBeginShape();
  for (float f=0; f < a - step + step; f+=step) {
    ofVertex(x,y);
    ofVertex(x + r*sin(rot + f), y + r*cos(rot + f));
    ofVertex(x + r*sin(rot + f+step), y + r*cos(rot + f+step));
  }
  ofEndShape();
}

void draw_endlerp(float t, float r = 100.0f, float a = 0.0f){
  float twist = -0.15f;
  float ox = end_radius * 0.5f;
  float oy = end_radius * 0.3f;
  draw_arc(t * ox, t * oy, r, a + t * twist);
  draw_arc(t * -ox, t * -oy, r, a + PI + t * -twist);
}


void draw_game(){

  // BACKGROND
  if(flash_time > 0)
  	flash_time--;
	ofBackground( (int) (200.0f * ((float) flash_time / flash_duration)) );


  float dt = ofGetLastFrameTime();
  if(state == GAME || state == UNLOCK_LERP){   
    // DOTS
    ofSetColor(255.0f * (1.0f - color_time));
    // move_dots(100);
    // draw_dots();
    point_cloud.update(&rects);
    point_cloud.draw();

    // FILLED
    if(filled != NONE){
      rects.at(filled).draw_filled(
        (dragged_rects.size() > 1 ? false : rects.at(filled).collides(mouse)), 
        1.0f - color_time
      );
    }

    // TARGET
    ofNoFill();
    ofSetLineWidth(1.0f);
    target.draw(255.0f * color_time);
    ofFill();
    target.draw(255.0f * color_time);

    // RECTS   
    if(dragged_rects.size() > 0){
      for(auto d : dragged_rects){
        d.rect->setPos(mouse.x - d.offset.x, mouse.y - d.offset.y);
      }
    }
    int ir = 0;
    for(auto r : rects){
      r.update(dt);
      if(filled != ir)
        r.draw(mouse, 1.0f - color_time);
      ir++;
    }
  }


// CIRCLE
  if(state != GAME && state != END_LERP){
    ofFill();
    draw_arc(0,0,end_radius, 0.0f, TWO_PI, 200.0f, 200.0f);
  }

  if(state == END_LERP){
    // draw_endlerp(0.0f);    
    ofFill();
    draw_endlerp(cam_clock.time, end_radius, end_angle);    
  }

 	// CROSS
  if(state == GAME || state == ENDING || state == END_LERP){
    velocity -= dt * 10.0f;
    if(velocity < 0)
    	velocity = 0;
  	cross.setPos(mouse.x, mouse.y);
    if(dragged_rects.size() == 0){
  	  cross.setRot(cross.r + velocity);
      stats.update_velocity(velocity);
    }
  	ofNoFill();
    float color = filled != NONE && rects.at(filled).collides(mouse) ? 0.0f : 180.0f * (1.0f - color_time);
    if(state == END_LERP)
      color = 255.0f;
   	cross.draw(color);

    if(state == ENDING){
      int l = stats.end_text.length();
      ofDrawBitmapString(stats.end_text,
        - (l / 2 ) * 8, 
        -10
      );
      draw_frame(-ofGetWidth() * 0.5f, -ofGetHeight() * 0.5f, false);
    }
  }
}

void update_lerps(float dt){
  float tt = 1.0f - cam_clock.time;
  cam_clock.update(dt, (tt * tt + 0.01f) * 40.0f);
  switch(cam_clock.state){
    case RUNNING : {
      switch(state){
        case UNLOCK_LERP : {
          lerp_unlock(cam_clock.time);
          color_time = cam_clock.time;
        }break;
        case END_LERP : {
          color_time = 1.0f;
          // lerp_unlock(1.0f - cam_clock.time);
        }break;
      }
    }
    break;

    case EOC :
      switch(state){
        case UNLOCK_LERP : 
          set_state(ENDING);
          end_clock.start(); 
        break;
        case END_LERP : reset_level(); break;
      }
    break;
  }
}

std::string hint_texts[] = {
  "together we came to get together we must group apart",
  "any new part comes off of the only wall",
  "sometimes try clicking with something",
  
  "beliefs will guide you but dare to stop streaming",
  "different floors had different keys but everyone was the same",
  "let x equal find x realize real things in return",

  "one of us was nowhere to be found, another was wearing a mask",
  "this one runs all day but never goes anywhere"
  
};
#define HINTS 8

int hint_index = 0;

void ofApp::draw(){
  w2 = ofGetWidth() * 0.5f;
  h2 = ofGetHeight() * 0.5f;
  float dt = ofGetLastFrameTime();

  if(state == END_LERP || state == UNLOCK_LERP) 
    update_lerps(dt);

  cam.begin();

  draw_game(); 
  
  cam.end();

  // HACK
  if(frame == 1){
  	reset_level();
		cam.disableMouseInput();
  }
  frame++;

  // FRAME
  if(middlemousedown || rightmousedown){
    reset_level();

    draw_frame();

    hint_clock.update(dt);
    if(hint_clock.state == EOC){
      hint_index = (hint_index + 1) % HINTS;
    }
    if(hint_clock.time >= 1.0f){
      int l = hint_texts[hint_index].length();
      ofSetColor(0);
      ofDrawBitmapString(hint_texts[hint_index], w2 - (l / 2) * 8, 20);
    }
  }

  if(SHOW_FPS)
    ofDrawBitmapString(std::to_string(fps.update(ofGetLastFrameTime())), 0, 20);

  if(SHOW_STATS)
    ofDrawBitmapString(stats.print(), 0, 36);

}

enum ShootResult{
  MISS,
  WRONG_ROT,
  HIT
};

int matching(std::vector<Dragged> *ds){
	int match = NONE;
	for(int i = 0; i< ds->size(); i++){
		if(match == NONE){
			match = (int) ds->at(i).rect->type;
		}else if(match != (int)ds->at(i).rect->type){
			return NONE;
		}
	}
	return match;
}

MatchType collection(std::vector<Dragged> *ds){
	assert(ds->size() == 3);
	int cs[] = {0, 0, 0, 0};
	for(int i = 0; i < 3; i++){
		cs[(int)ds->at(i).rect->type]++;
	}

	bool r = true;
	for(int i = 0; i<3; i++){
		if(cs[i] != 1)
			r = false;
	}


	if(r){
		return MATCH_SET;
	}else{
		r = false;
		for(int i = 0; i<3; i++){
			if(cs[i] == 3)
				r = true;
		}
		if(r)
			return MATCH_TRI;
		else
			return NO_MATCH;
	}
}
int wrap(int i, int c){
	if(i >= c){
		return i % c;
	}else if(i < 0)
		return c + i;
	else
		return i;
}

MatchType get_match(std::vector<Dragged> *ds){
	int stack = ds->size();
	switch(stack){
		case 0 : 
			return NO_MATCH;
		case 2 : {
			int match = matching(ds);
			if(match > NONE){
				return MATCH_PAIR;
			}else
				return NO_MATCH;
		}break;

		case 3 : {
			return collection(ds);
		}break;

		case 4 :
			return NO_MATCH_FULL;
		break;

		default : return NO_MATCH;
	}
}
void reset_pos(std::vector<Dragged> *ds){
	for(int i = 0; i<3; i++){
		ds->at(i).rect->setPos(rx(), ry());
	}
	// ds->clear();
}
int get_fourth(std::vector<Dragged> *ds){
  bool inside[] = {false, false, false, false};
  for(int i = 0; i<3; i++){
    inside[ds->at(i).id] = true;
  }
  for(int r = 0; r<4; r++){
    if(!inside[r])
      return r;
  }

  return NONE;
}
void reset_filled(int i){
  rects.at(i).setPos(rx(), ry());
  filled = NONE;
  // rects.at(filled).type = floor(ofRandom(0,2.1f));
}
void randomize_rects(std::vector<Dragged> *rs, bool forward = false){
  float r = 300.0f * (0.5f + ofRandom(0.0f, 0.5f)) * (ofRandom(0, 100) < 50 ? 1.0f : -1.0f);
  for(int i = 0; i<rs->size(); i++){
    rs->at(i).rect->move(APP_WIDTH, APP_HEIGHT, ofRandom(-r, r));
    rs->at(i).rect->rotate(
      (forward ? 70.0f : -1.0f * (20.0f + ofRandom(0.0f, 50.0f))) 
    );
  }
}
void click_target(ofVec3f m){
  ofVec2f d = ofVec2f(m.x - target.x, m.y - target.y);
  float dist = sqrt(d.x * d.x + d.y * d.y);
  ShootResult r = (dist < 7.0f) ? (cross.aligned(target.r) ? HIT : WRONG_ROT) : MISS;
  switch(r){
    case MISS : {
      reset_filled(filled);
    }break;

    case WRONG_ROT : {
      //TODO red ?
      reset_filled(filled);
    }break;

    case HIT : {
      set_state(UNLOCK_LERP, true);
      stats.validate(filled);
    }break;
  }
}


ofVec2f click_start = ofVec2f(0,0);


void click_game(int x, int y){
	dragged_rects.clear();
	int i = 0;
	for(auto r : rects){
		if(r.collides(mouse))
			dragged_rects.push_back(Dragged(i, &rects.at(i), ofVec3f(mouse.x - r.x, mouse.y - r.y, 0)));
		i++;
	}
  click_start.x = mouse.x;
  click_start.y = mouse.y;

	if(dragged_rects.size() > 1){
		MatchType m = get_match(&dragged_rects);
		switch(m){
			case MATCH_PAIR : {
				int match = dragged_rects.at(0).rect->type;
				dragged_rects.at(0).rect->type = (RectType)wrap(match + 1, 3);
				dragged_rects.at(1).rect->type = (RectType)wrap(match - 1, 3);
			}break;

			case MATCH_TRI :{ 
				flash_time = flash_duration; 
        flashed = true;
			}break;

			case MATCH_SET :{
        filled = get_fourth(&dragged_rects);
			}break;

			case NO_MATCH_FULL : {
				reset_level();
			}break;

      case NO_MATCH : {
      }break;

			default : 

      break;
		}

    stats.update_match(m);

		if(m != NO_MATCH){
			dragged_rects.clear();
		}
    if(m != MATCH_SET)
      filled = NONE;

	}else if(dragged_rects.size() == 1 && dragged_rects.at(0).id == filled){
    click_target(mouse);
  }
}
void click_end(int x, int y){
  set_state(END_LERP, true);
  // end_angle = ((float)((int) cross.r % 90) / 90.0f) * PI * 0.5f;
}
void ofApp::mousePressed(int x, int y, int button){
  if (button == 0){
    switch(state){
      case GAME : 
      click_game(x, y);
      break;

      case ENDING :
      click_end(x, y);
      break;
    }
  }
  if (button == 2){
    rightmousedown = true;
    // resetting = true;
    hint_clock.start();
  }
  if (button == 1) {
  	middlemousedown = true;
    hint_clock.start();
  }
  reset_idle();
}
void ofApp::mouseReleased(int x, int y, int button){
  velocity = 0.0f;
  if (button == 0) {
  	if(dragged_rects.size() > 1 && state == GAME && click_start.distance(mouse) < 10.0f){
      int match = get_match(&dragged_rects);
      switch(match){
        case NO_MATCH : 
        randomize_rects(&dragged_rects);
        break; 
      }
    }

    if(dragged_rects.size() > 0 && state == GAME && click_start.distance(mouse) > 10.0f){
      stats.update_drags();
    }

  	dragged_rects.clear();   
    leftmousedown = false;
    leftmouseup = true;
  }
  if (button == 2) {
    rightmousedown = false;
    rightmouseup = true;
    // resetting = false;
  }
  if(button  == 1){
  	middlemousedown = false;
  }
  reset_idle();
}
void ofApp::keyPressed(int key){
  switch(key){
    case 'x' :{
      img.grabScreen(0, 0, ofGetWidth(), ofGetHeight());
      img.save("moly-" + std::to_string(screenshots) + ".png");
      screenshots++;
    }break;

    case 't' : {
    	flash_time = flash_duration;
    }break;

    case 'e' : {
      set_state(UNLOCK_LERP, true);
      stats.validate(0);
    }break;


    case 's' : {
      SHOW_STATS = !SHOW_STATS;
    }break;

    case 'r' : {
      set_state(END_LERP, true);
    }break;
  }
}

ofVec3f last_mouse;


void set_velocity(int x, int y){
  float v = (abs(last_mouse.x - x) + abs(last_mouse.y - y)) * 0.01f;
	velocity += v;
	last_mouse.x = x;
	last_mouse.y = y;

}
void ofApp::mouseMoved(int x, int y ){
	set_velocity(x, y);
  reset_idle();
}
void ofApp::keyReleased(int key){}
void ofApp::mouseDragged(int x, int y, int button){
	set_velocity(x, y);
  reset_idle();
}
void ofApp::mouseEntered(int x, int y){}
void ofApp::mouseExited(int x, int y){}
void ofApp::windowResized(int w, int h) {}
void ofApp::gotMessage(ofMessage msg){}
void ofApp::dragEvent(ofDragInfo dragInfo) {} 



/*

screen agnostic rect sizes

equal cross scaling

*/
