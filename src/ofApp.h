#pragma once

#include "ofMain.h"

#define APP_WIDTH 1280 
#define APP_HEIGHT 1024


#define DOTS 4096
#define NONE -1
#define SHOW_TITLE false



enum ClockState{
  STOPPED,
  RUNNING,
  EOC
};
struct LerpClock{
  ClockState state = STOPPED;
  float time = 0.0f;
  float speed = 1.0f;
  LerpClock(float s = 1.f){
    speed = s;
  }
  float update(float dt, float mult = 1.f){
    switch(state){
      case RUNNING :
        time = time + dt * speed * mult;
        if(time >= 1.0f){
          time = 1.f;
          state = EOC;
        }
      break;

      case EOC :
        state = STOPPED;
      break;
    
    }
    return time;
  }
  void start(){
    state = RUNNING;
    time = 0.0f;
  }
};


struct Player{
	ofVec2f pos;
	float rot;
};

enum MatchType{
  NO_MATCH_FULL,
  NO_MATCH,
  MATCH_PAIR,
  MATCH_TRI,
  MATCH_SET
};

enum Ending{
	SMALL,
	FLASHY,
	BIG,
	LONGS,

	NO_FLASH,
	NO_FLASH_LUCK,
	ROTATOR,
	ROUTINE,
	
	PERFECT_FLAT,
	PERFECT,
	
	ENDINGS_COUNT
};


enum FormType{
  APRO,
  NAGY,
  HOSSZ,
  HOSSZU
};

struct Stats{
	int nomatches, pairs, flashes, fills, drags;
	FormType filled;

	Ending ending;

	std::string end_text;

	float turns;

	int minimum_clicks = 9;

	std::string end_texts[(int) ENDINGS_COUNT] = {
		"you had the right map but the streets would never turn out to be so flat",
		"it could have been strong but all was built on a hole",
		"it had plenty of space for you but you could never grow big enough to fill it",
		"it was all too nice and it never really fit you",

		"",
		"they must have been lucky and oblivious. you wondered if it was their first time",
		"you took your time and it was gone by the end. you understood the price you had to pay",
		"it has been simple from the beginning. it quickly turned into a routine and a fall",
		
		"almost had it all but still missed it because you were looking around too much",
		"you obsessed about it so much it became scary or lame. it is time to let go"
	};
	void update_drags(){
		drags++;
	}
	void update_match(MatchType m){
		switch(m){
			case NO_MATCH :
			nomatches++; break;
			case MATCH_PAIR : 
			pairs++; break;
			case MATCH_TRI : 
			flashes++; break;
			case MATCH_SET : 
			fills++; break;

			default: break;
		}
	}

	void update_velocity(float x){
		turns += x;
	}

	int get_clicks(){
		return nomatches + pairs + flashes + drags;
	}

	void validate(int f){
		int clicks = get_clicks();

		filled = (FormType) f;

		if(clicks <= minimum_clicks){
			if(filled == APRO){
				if(turns <= 92)
					ending = PERFECT;
				else
					ending = PERFECT_FLAT;
			}else{
				ending = ROUTINE;
			}
		}else if(flashes == 0){
			if(clicks <= minimum_clicks)
				ending = NO_FLASH_LUCK;
			else
				ending = NO_FLASH;
		}else if(flashes >= 4){
			ending = FLASHY;
		}else if(nomatches >= 4){
			ending = ROTATOR;
		}else{
			switch(f){
				case APRO :{
					ending = SMALL;
				}break;
				case NAGY :{
					ending = BIG;
				}break;
				default : {
					ending = LONGS;
				}
				break;
			}
		}
		end_text = end_texts[(int) ending];
	}

	std::string print(){
		return "turns : " + std::to_string(turns) + "\nclicks : " + std::to_string(get_clicks());
	}
};


struct Mat4 : ofMatrix4x4{
	Mat4(float x = 0, float y = 0, float w = 1, float h = 1, float r = 0){
		set(x, y, w, h, r);
	}
	void set(float x = 0, float y = 0, float w = 1, float h = 1, float r = 0){
		ofMatrix4x4::glTranslate( x, y, 0 );
		ofMatrix4x4::glRotate( r, 0, 0, 1 );
		ofMatrix4x4::glScale( w, h, 1 );
	}
};


enum RectType{
	EMPTY,
	DOTTED,
	STRIPES,
	FILL
};

struct Rect : ofRectangle{
	float x, y, w, h, r, w2, h2;
	ofColor color;
	Mat4 matrix, base_matrix, target_matrix, running_matrix;
	RectType type;

	Mat4 *current;

	LerpClock clock;
	ofVec2f vel;

	Rect(float _x, float _y, float _w, float _h, float _r = 0, RectType t = EMPTY){
		setSize(2,2);
		setPosition(-1, -1);
		type = t;

	  setMatrix(_x, _y, _w, _h, _r);
	
	}

	void add_force(ofVec2f f){
		vel.x += f.x;
		vel.y += f.y;
	}
	void start_lerp(){
		current = &running_matrix;
		clock.start();
	}
	void change_type(int t){
		type = (RectType) t;
		start_lerp();
	}
	void updateMatrix(){
	  matrix = Mat4(x, y, w, h, r);
	}

	void setMatrix(float _x, float _y, float _w, float _h, float _r = 0){
		x = _x; y = _y; w = _w; h = _h; r = _r;
	  updateMatrix();
	}
	ofVec2f getRandomPos(float width, float height){
		return ofVec2f(ofRandom(width * -0.42f, width * 0.42f), ofRandom(height * -0.42f, height * 0.42f));
	}
	void move(float _w, float _h, float ran){
		ofVec2f p = ofVec2f(x, y).interpolate(getRandomPos(_w, _h), ran);
		// x = p.x;
		// y = p.y;
		updateMatrix();

	}
	void setPos(float _x, float _y, float _z = 0){
		x = _x; y = _y;
		updateMatrix();
	}
	void rotate(float d){
		r += d;
		updateMatrix();	
	}
	void setRot(float _r){
		r = _r;
		updateMatrix();
	}

	bool collides(ofVec3f mouse){
		return ofRectangle::inside( mouse * matrix.getInverse() );
	}
	void draw_filled(bool highlight, float alpha = 1.0f){
		ofPushMatrix();
		ofPushStyle();

		if(highlight)
			ofSetColor(212 * alpha);
		else
			ofSetColor(180 * alpha);
		// }else{
		// if(!fill)
		ofFill();
		// else
		// }


		ofSetLineWidth(0.0f);

		ofMultMatrix(matrix);

		ofSetPolyMode(OF_POLY_WINDING_ODD);
		ofBeginShape();
		float s = 1.0f;
		ofVertex(-s,-s);
		ofVertex(s,-s);
		ofVertex(s,s);
		ofVertex(-s,s);
		ofEndShape(true);
		ofPopMatrix();  
		ofPopStyle();
	}		
	void draw(ofVec3f mouse, float alpha = 1.0f){
		ofPushMatrix();
		ofPushStyle();

		if(collides(mouse))
			ofSetColor(122.0f * alpha);
		else
			ofSetColor(222.0f * alpha);

		ofNoFill();

		ofSetLineWidth(1.0f);

		ofMultMatrix(matrix);

		ofSetPolyMode(OF_POLY_WINDING_ODD);

		switch(type){
			case STRIPES : {
				float c = 20.0f;
				for(float i = 0; i < c; i += 1.0f){
					float ww = (i / c) * 2.0f - 1.0f;
					ofDrawLine(ww, -1.f, ww, 1.f);
				}
			}break;

			default : 
			break;
		}
		ofBeginShape();
		float s = 1.0f;
		ofVertex(-s,-s);
		ofVertex(s,-s);
		ofVertex(s,s);
		ofVertex(-s,s);
		ofEndShape(true);

		ofPopMatrix();  
		ofPopStyle();
	}

	void update(float dt){
		clock.update(dt);
		switch(clock.state){
			case STOPPED : 
			break;

			case RUNNING :
				// current->lerp_from(&base_matrix, &target_matrix, clock.time);
			break;

			case EOC :{
				base_matrix = target_matrix;
				target_matrix = Mat4();
				current = &base_matrix;
			}break;
		}
	}
};

struct PointCloud{
	ofMesh points;
	std::vector<bool> states;
	
	const ofFloatColor black = ofFloatColor(0,0,0);
	const ofFloatColor white = ofFloatColor(1,1,1);

	int w2, h2, width, height, count;

	float rx(){
		return ofRandom(-w2, w2);
	}

	float ry(){
		return ofRandom(-h2, h2);
	}
	void set_size(int c = DOTS, int w = APP_WIDTH, int h = APP_HEIGHT){
		count = c;
		w2 = w / 2;
		h2 = h / 2;
		width = w;
		height = h;
	}
	void init(int c = DOTS, int w = APP_WIDTH, int h = APP_HEIGHT){
		set_size(c, w, h);
	  for(int i = 0; i<c; i++){
	    points.addVertex(ofPoint(rx(), ry(), 0));
	    states.push_back(true);
	    points.addColor(ofFloatColor(1,1,1));
	  }
	}

	void randomize(){
	  for(int i = 0; i<count; i++){
	    points.setVertex(i, ofPoint(rx(), ry(), 0));
	    states[i] = false;
	    points.setColor(i, black);
	  }
	}


	void move_vertex(int index, float f = 2.0f){
	  ofVec3f v = points.getVertex(index);
	  v.x += ofRandom(-f, f);
	  v.y += ofRandom(-f, f);

	  if(v.x > width)
	    v.x = -width;
	  else if(v.x < -width)
	    v.x = width;
	  if(v.y > height)
	    v.y = -height;
	  else if(v.y < -height)
	    v.y = height;
	  
	  points.setVertex(index, v);
	}

	void vibrate_vertex(int index, float f = 10.0f){
	  ofVec3f v = points.getVertex(index);
	  v.z = ofRandom(-f, f);
	  points.setVertex(index, v);
	}

	inline void activate_point(int i){
	  if(!states[i]){
	    states[i] = true;
	    points.setColor(i, white);
	  }
	}

	void update(std::vector<Rect> *rects){
	  int dotted_collides = 0;  
	  for(int i = 0; i<count; i++){
	    for(int j = 0; j<4; j++){
	      if(rects->at(j).type == DOTTED && rects->at(j).collides(points.getVertex(i)))
	        dotted_collides += 1;
	    }
	    switch(dotted_collides){
	      case 0 :
	      if(states[i]){
	        states[i] = false;
	        points.setColor(i, black);
	      }break;

	      case 1 :
	      activate_point(i);
	      if(ofRandom(0, count) < 400)
	        move_vertex(i);
	      break;

	      case 2 : 
	      activate_point(i);
	      // vibrate_vertex(i);
	      move_vertex(i, 2.0f);
	      break;


	      case 3 : 
	      activate_point(i);
	      move_vertex(i, 5.0f);
	      break;

	      default :
	      break;
	    }

	    dotted_collides = 0;
	  }
	}

	void draw(){
		points.drawVertices();
	}
};

struct Dragged{
	ofVec3f offset;
	Rect *rect = NULL;
	int id = -1;
	Dragged(int i, Rect *r, ofVec3f o){
		rect = r; offset = o; id = i;
	}
};
struct FpsCounter{
	int index = 0, length = 5;
	std::vector<float> fps;
	FpsCounter(int l){
		length = l;
		for(int i = 0; i<length; i++)
			fps.push_back(0.0f);
	}
	int update(float dt){
		fps[index] = dt;
		index = (index + 1) % length;
		float average = 0;
		for(auto f : fps)
			average += f;
		return floor((average / ((float) length) * 3600.0f));
	}
};
struct Cross {
	float x, y, s, r;
	Mat4 matrix;
	float w = 0.3f;
	// ofColor color;
	void updateMatrix(){
		float s1 = s;
		float s2 = s;
		if( (int)floor(( (float)(((int)floor(r + 53.f)) % 360) / 360.0f) * 4.0f) % 2 == 0)
			s1 *= 0.8f;
		else
			s2 *= 0.8f;
		matrix = Mat4(x, y, s1, s2, r);
	}
	Cross(float _x = 0, float _y = 0, float size = 9.0f, float rot = 0, int c = 180){
		x = _x; y = _y; s = size; r = rot;
		updateMatrix();
		// setColor(c);
	}
	bool aligned(float t){
		int _r = (int)floor(r) % 360;
		int _t = (int)floor(t) % 360;
		int d = abs(_r - _t);
		d = d % 90;
		return (d < 10 || d > 80);
	}
	void setPos(float _x, float _y){
		x = _x; y = _y;
		updateMatrix();
	}
	void setSize(float _s){
		s = _s;
		updateMatrix();
	}
	// void setColor(int c){
	// 	color = ofColor(c);
	// }
	void setRot(float _r){
		r = _r;
		updateMatrix();
	}
	void draw(float color){
		ofSetColor(color);
		ofPushMatrix();
		// ofPushStyle();
		ofSetLineWidth(1.0f);

		ofSetPolyMode(OF_POLY_WINDING_ODD);

		ofMultMatrix(matrix);

		ofBeginShape();
		ofVertex(-s,-w);
		ofVertex(-w,-w);
		ofVertex(-w,-s);
		ofVertex( w,-s);
		ofVertex( w,-w);
		ofVertex( s,-w);
		ofVertex( s, w);
		ofVertex( w, w);
		ofVertex( w, s);
		ofVertex(-w, s);
		ofVertex(-w, w);
		ofVertex(-s, w);
		ofEndShape(true);

		ofPopMatrix();  
		// ofPopStyle();
	}
};

class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void mouseEntered(int x, int y);
		void mouseExited(int x, int y);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);
		
};
